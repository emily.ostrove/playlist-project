from django.contrib import admin
from songs.models import Song, Artist, Album, Playlist

# Register your models here.
class SongAdmin(admin.ModelAdmin):
    pass

class ArtistAdmin(admin.ModelAdmin):
    pass

class AlbumAdmin(admin.ModelAdmin):
    pass

class PlaylistAdmin(admin.ModelAdmin):
    pass

admin.site.register(Song, SongAdmin)
admin.site.register(Artist, ArtistAdmin)
admin.site.register(Album, AlbumAdmin)
admin.site.register(Playlist, PlaylistAdmin)