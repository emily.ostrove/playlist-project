from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as auth_views

from songs.views import(
    SongsListView,
    SongsCreateView,
    PlaylistListView,
    PlaylistCreateView,
    PlaylistDetailView,
    ArtistListView,
    ArtistCreateView,
    ArtistDetailView,
    search_songs
)

urlpatterns = [
    path("", SongsListView.as_view(),name="home"),
    path("add/", SongsCreateView.as_view(),name="add_song"),
    path("playlist/", PlaylistListView.as_view(), name="all_playlists"),
    path("playlist/new/", PlaylistCreateView.as_view(), name="new_playlist"),
    path("playlist/<int:pk>/",PlaylistDetailView.as_view(), name="playlist_details"),
    path("artists/", ArtistListView.as_view(), name="all_artists"),
    path("artists/add/", ArtistCreateView.as_view(), name="new_artist"),
    path("artists/<int:pk>/", ArtistDetailView.as_view(), name="artist_detail"),
    path("login/", auth_views.LoginView.as_view(), name="login"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("search_songs/", search_songs, name="search-songs"),
]