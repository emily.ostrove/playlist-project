from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.
class Song(models.Model):
    name = models.CharField(max_length=80)
    artist = models.ForeignKey(
        "Artist",
        related_name="songs",
        on_delete=models.CASCADE,
        null=True,
    )
    image = models.URLField(null=True, blank=True)
    album = models.ForeignKey(
        "Album",
        related_name="songs",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
  

    # this updates the name in the admin
    def __str__(self):
        return self.name
    

class Artist(models.Model):
    name = models.CharField(max_length=50)
    # album = models.ForeignKey(
    #     "Albums",
    #     related_name="album",
    #     on_delete=models.CASCADE,
    #     null=True,
    # )

    # this updates the name in the admin
    def __str__(self):
        return self.name

class Album(models.Model):
    name = models.CharField(max_length=50)
    record_label = models.CharField(max_length=50)


class Playlist(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="playlist",
        on_delete=models.CASCADE,
        null=True,
    )
    song = models.ManyToManyField(
        "Song",
        related_name="songs",
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.name
    