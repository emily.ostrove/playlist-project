from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login

from songs.models import Song, Artist, Album, Playlist
# Create your views here.

class SongsListView(ListView):
    model = Song
    template_name = "songs/list.html"


class SongsCreateView(CreateView):
    model = Song
    template_name = "songs/new.html"
    fields = ["name","artist","album","image"]
    
    def get_success_url(self):
        return reverse_lazy("home")
    
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

class ArtistListView(ListView):
    model = Artist
    template_name = "artists/list.html"
    fields = ["name"]


class ArtistCreateView(CreateView):
    model = Artist
    template_name = "artists/new.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("all_artists")


class ArtistDetailView(DetailView):
    model = Artist
    template_name = "artists/detail.html"
    fields = ["name", "song"]
    success_url = reverse_lazy("home")


class PlaylistListView(ListView):
    model = Playlist
    template_name = "playlists/list.html"


class PlaylistCreateView(CreateView):
    model = Playlist
    template_name = "playlists/new.html"
    fields = ["name", "owner","song"]

    def get_success_url(self):
        return reverse_lazy("all_playlists")

class PlaylistDetailView(DetailView):
    model = Playlist
    template_name ="playlists/detail.html"
    fields = ["name", "owner", "song"]

    def get_success_url(self):
        return reverse_lazy("all_playlists")

class AlbumListView(ListView):
    model = Album


class AlbumCreateView(CreateView):
    model = Album


def sign_up(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            usernameAPP = request.POST.get("username")
            passwordAPP = request.POST.get("password1")
            # User is a model with a function create_user
            # setting our variables usernameAPP and passwordAPP
            # to the create_user parameters
            user = User.objects.create_user(
                username=usernameAPP, password=passwordAPP
            )
            user.save()
            login(request, user)
            return render(request, "home")
    else:
        form = UserCreationForm(request.POST)
    context = {"form": form}
    return render(request, "registration/sign_up.html", context)


def search_songs(request):
    if request.method == "POST":
        searched = request.POST.get('searched')
        songs = Song.objects.filter(name__contains=searched)
        artists = Artist.objects.filter(name__contains=searched)
        playlists = Playlist.objects.filter(name__contains=searched)
        return render(request, 'songs/search.html',
        {'searched':searched, 'songs':songs, 'artists':artists, 'playlists':playlists})
    else:
        return render(request, 'songs/search.html',
        {})